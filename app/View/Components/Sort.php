<?php

namespace App\View\Components;

use App\Buffer;
use Illuminate\View\Component;
use App\Sort\TypeSorter;

class Sort extends Component
{
    public $arResult;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {

        $typeSort = new TypeSorter();
        $this->arResult["TYPE_SORT"] = $typeSort->getTypeSorter();

        $this->getData();
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.sort');
    }

    private function getData()
    {
        foreach ($this->arResult["TYPE_SORT"] as $type) {
            $this->arResult["SORT_DATA"][$type] = $this->getBbufferedData($type);
        }
    }

    private function getBbufferedData($type): array
    {
        $buffered = Buffer::getInstace();
        return $buffered->getBuffer($type);
    }

    public function getJson()
    {
        foreach ($this->arResult["SORT_DATA"] as $key => $item) {
            $this->arResult["SORT_DATA"][$key]["DATA"] = array_chunk($item["DATA"], $item["SIZE"]);
        }
        return $this->arResult;
    }
}
