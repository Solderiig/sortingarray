<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Menu extends Component
{
    /**
     * @var array
     */
    public array $arResult;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->arResult = [
            "Web",
            "File",
            "DB",
        ];
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.menu');
    }

    public function getJson()
    {
        return $this->arResult;
    }
}
