<?php

namespace App\View\Components;

use App\Sort\TypeSorter;
use Illuminate\View\Component;

class Filter extends Component
{
    public $arResult;
    public $idForm;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($formID)
    {
        $this->idForm = $formID;

        $typeSort = new TypeSorter();
        $this->arResult["TYPE_SORT"] = $typeSort->getTypeSorter();

        $this->arResult["SIZE"]["MIN"] = 2;
        $this->arResult["SIZE"]["MAX"] = 10;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.filter');
    }
}
