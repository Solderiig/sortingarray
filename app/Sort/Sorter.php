<?php

namespace App\Sort;

interface Sorter
{
    public function sort();
}
