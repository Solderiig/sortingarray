<?php

namespace App\Sort;

class VerticalSort extends AbstractSort implements Sorter
{

    public function sort(): array
    {
        $chunks = array_chunk($this->array, $this->getSize());

        $chunks_r = [];
        foreach ($chunks as $i => $chunk) {
            foreach ($chunk as $j => $el) {
                $chunks_r[$j][$i] = $el;
            }
        }

        return $this->meargeArray($chunks_r);
    }
}
