<?php

namespace App\Sort;

use App\DB\DbEntityManager,
    App\Viewer\Observer;

class TypeSorter
{
    private array $typeSorter;

    public function __construct()
    {
        $this->typeSorter = [
            "Horizontal",
            "Vertical",
            "Snake",
            "Diagonal",
            "Snail",
        ];
    }

    public function runSort(array $array, Observer $viever, int $size): void
    {
        foreach ($this->getTypeSorter() as $item) {
            try {
                $sorter = FactorySorter::initial($item);
            } catch (\ErrorException $e) {
                echo 'Помилка створення об`єкту сортувальника ', $e->getMessage(), "\n";
            }

            $sorter->setArray($array);
            $sorter->setSize($size);

            $arResult = $sorter->sort();
            $viever->print($arResult, $item);
        }
    }

    public function getTypeSorter(): array
    {
        $this->checkRequest();

        return $this->typeSorter;
    }

    private function checkRequest(): void
    {
        if (!empty(\request()->get("typeSort"))) {
            foreach ($this->typeSorter as $key => $item) {
                if (!in_array($item, \request()->get("typeSort"))) {
                    unset($this->typeSorter[$key]);
                }
            }
        }
    }
}
