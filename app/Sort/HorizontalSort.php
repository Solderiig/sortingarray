<?php

namespace App\Sort;

class HorizontalSort extends AbstractSort implements Sorter
{
    public function sort(): array
    {
        return $this->array;
    }
}
