<?php

namespace App\Sort;

class SnakeSort extends AbstractSort implements Sorter
{
    public function sort(): array
    {
        $chunks = array_chunk($this->array, $this->getSize());

        $chunks_r = [];
        foreach ($chunks as $i => $chunk) {
            if ($i % 2 == 1) {
                $chunks_r[$i] = array_reverse($chunk);
            } else {
                $chunks_r[$i] = $chunk;
            }
        }

        return $this->meargeArray($chunks_r);
    }
}
