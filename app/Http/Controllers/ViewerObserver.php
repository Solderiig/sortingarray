<?php

namespace App\Http\Controllers;

use App\Viewer\Observer;
use App\Viewer\FactoryViewer;
use App\Viewer\AbstractViewer;
use Psy\Exception\ErrorException;

class ViewerObserver extends Controller
{
    /**
     * @var Observer
     */
    private Observer $observerViewed;
    private array $array;
    private int $size;
    private array $selectViever;

    public function setArray($array): void
    {
        $this->array = $array;
    }

    public function setSize(int $size): void
    {
        $this->size = $size;
    }

    public function getObserver(): Observer
    {
        $this->observerViewed = new Observer();
        $action = \Request::route()->getName();

        $actionRequest = \request()->get("action");

        if ($actionRequest) {
            $action = $actionRequest;
        }

        $func = $action . "Action";

        try {
            if (method_exists($this, $func)) {
                $this->$func();
            } else {
                $this->defaulAction();
            }

            $this->attachViever();
        } catch (\ErrorException $e) {
            echo 'Помилка виводу: ', $e->getMessage(), "\n";
        };

        return $this->observerViewed;
    }

    private function FileAction(): void
    {
        $this->pushViewer(AbstractViewer::FILE);
    }

    private function DBAction(): void
    {
        if (!empty(\request()->get("send"))) {
            $this->pushViewer(AbstractViewer::DB);
        }
    }

    private function AllAction(): void
    {
        $this->pushViewer(AbstractViewer::WEB);
        $this->pushViewer(AbstractViewer::FILE);
        $this->pushViewer(AbstractViewer::DB);
    }

    private function defaulAction(): void
    {
        $this->pushViewer(AbstractViewer::WEB);
    }

    private function pushViewer(string $type): void
    {
        $this->selectViever[$type] = $type;
    }

    private function attachViever()
    {
        if (!empty($this->selectViever)) {
            foreach ($this->selectViever as $viewerItem) {
                $viewerFile = FactoryViewer::initial($viewerItem);
                $viewerFile->setSize($this->size);
                $viewerFile->setDefaultArray($this->array);
                $this->observerViewed->attach($viewerFile);
            }
        } else {
            throw new ErrorException("Empty Viewer observer");
        }
    }
}
