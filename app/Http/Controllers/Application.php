<?php

namespace App\Http\Controllers;

use App\Sizer;
use App\GenerateArray;
use App\Viewer\Observer;
use App\Sort\TypeSorter;
use App\Sort\NormalizeArray;

class Application extends Controller
{
    private Observer $observerViewed;
    private int $size;
    private array $array;
    private array $normalizeArray;

    public function run()
    {
        $this->size = $this->getSize();
        $this->array = $this->generateArray();

        $this->normalizeArray();
        $this->coreRun();
        $this->sort();

        return $this->frontAction();
    }

    private function getSize(): int
    {
        $size = Sizer::getInstance();

        $sizeValue = null;

        if (\request()->get("size") != "") {
            $sizeValue = \request()->get("size");
        }

        $size->setSize($sizeValue);
        $intSize = $size->getSize();

        return $intSize;
    }

    private function generateArray(): array
    {
        $generateArray = new GenerateArray($this->size);
        $array = $generateArray->generate();

        return $array;
    }

    private function normalizeArray(): void
    {
        $normalize = new NormalizeArray($this->array);
        $this->normalizeArray = $normalize->get();
    }

    private function sort(): void
    {
        $sorter = new TypeSorter();
        $sorter->runSort($this->normalizeArray, $this->observerViewed, $this->size);
    }

    private function coreRun(): void
    {
        $this->setViewer();
    }

    private function setViewer(): void
    {
        $controller = new ViewerObserver();
        $controller->setSize($this->size);
        $controller->setArray($this->array);
        $this->observerViewed = $controller->getObserver();
    }

    private function frontAction()
    {
        $action = \Request::route()->getName();
        $frontController = new \App\Front\Controller($action);
        return $frontController->run();
    }
}
