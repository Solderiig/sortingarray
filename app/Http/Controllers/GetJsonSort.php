<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\View\Components\Sort;

class GetJsonSort extends Controller
{
    public function getJson(): array
    {
        $app = new Application();
        $app->run();

        $actionRequest = \request()->get("action");

        $data = [true];

        if (!$actionRequest || $actionRequest == "paramsTypeSort") {
            $comp = new Sort();
            $data = $comp->getJson();

        }

        return $data;
    }
}
