<?php

namespace App\Front;

use Illuminate\View\View;

class Controller
{
    const DEFAULT = "default";
    const WEB = "Web";
    const FILE = "File";
    const DB = "DB";
    const SETPARAMS = "setParams";
    const GET_JSON_SORT = "getSortJson";

    private string $type;

    public function __construct(string $type = self::DEFAULT)
    {
        $this->type = $type;
    }

    public function run(): View
    {
        switch ($this->type) {
            case self::DEFAULT:
            case self::WEB:
                return view("index");
                break;
            case self::FILE:
                return view("file");
                break;
            case self::DB:
                return view("db");
                break;
            case self::SETPARAMS:
                return view("ajax");
                break;
            case self::GET_JSON_SORT:
                return view("ajax");
                break;
        }
    }
}
