<?php

namespace App\Crowdin;

class App
{
    public function manifest(): array
    {
        return [
            "identifier" => "sort_app_serhii",
            "name" => "SortSerhii",
            "description" => "SortSerhii",
            "logo" => "/filter_and_sort.png",
            "baseUrl" => "https://3f05-5-58-148-58.eu.ngrok.io/",
            "authentication" => [
                "type" => "authorization_code",
                "clientId" => env("CROWDIN_APP_CLIENT_ID"),
            ],
            "events" => [
                "installed" => "/install",
            ],
            "scopes" => [
                "project",
            ],
            "modules" => [
                "project-integrations" => [
                    [
                        "key" => "sort_app_serhii_module",
                        "name" => "Demo Sort Serhii",
                        "description" => "Demo Sort Serhii",
                        "logo" => "/filter_and_sort.png",
                        "url" => "/",
                        "environments" => [
                            "crowdin",
                        ],
                    ],
                ],
            ],
        ];
    }

    public function event(): array
    {
        return ['success' => true];
    }
}
