<?php

namespace App\Viewer;

use SplSubject;
use SplObjectStorage;
use App\Viewer\AbstractViewer;

class Observer implements SplSubject
{
    private SplObjectStorage $observers;
    private array $array;
    private string $sorter;

    public function __construct()
    {
        $this->observers = new SplObjectStorage();
    }

    public function attach($observer): void
    {
        $this->observers->attach($observer);
    }

    public function detach($observer): void
    {
        $this->observers->detach($observer);
    }

    public function print(array $array, string $sorter): void
    {
        $this->array = $array;
        $this->sorter = $sorter;
        $this->notify();
    }

    public function notify(): void
    {
        foreach ($this->observers as $observer) {
            $observer->print($this->array, $this->sorter);
        }
    }
}
