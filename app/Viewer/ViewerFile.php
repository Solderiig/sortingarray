<?php

namespace App\Viewer;

class ViewerFile extends AbstractViewer
{
    const FILE_LOG = 'file_result.txt';
    const DIR = '/file/';

    protected int $size;

    public function __construct()
    {
        $file = $_SERVER["DOCUMENT_ROOT"] . self::DIR . self::FILE_LOG;

        if (file_exists($file)) {
            unlink($file);
        }
    }

    public function print($array, string $sorter): void
    {
        $arResult = array_chunk($array, $this->GetSize());

        $resString = $sorter . PHP_EOL;

        foreach ($arResult as $item) {
            $resString .= implode("\t", $item) . PHP_EOL;
        }

        $resString .= PHP_EOL . PHP_EOL;

        $this->CheckDir();

        file_put_contents($_SERVER["DOCUMENT_ROOT"] . self::DIR . self::FILE_LOG, $resString, FILE_APPEND);
    }

    private function CheckDir()
    {
        $dir = $_SERVER["DOCUMENT_ROOT"] . self::DIR;

        if (!file_exists($dir) && !is_dir($dir)) {
            mkdir($dir);
        }
    }
}
