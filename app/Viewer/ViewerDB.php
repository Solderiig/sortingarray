<?php

namespace App\Viewer;

use App\Models\Result;

class ViewerDB extends AbstractViewer
{
    public function print(array $array, string $sorter): void
    {
        $result = new Result();

        $result->in_array = json_encode($this->getDefaultArray());
        $result->out_array = json_encode($array);
        $result->type_sorter = $sorter;

        $result->save();
    }
}
