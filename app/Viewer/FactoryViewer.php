<?php

namespace App\Viewer;

class FactoryViewer
{
    public static function initial(string $type): AbstractViewer
    {
        switch ($type){
            case AbstractViewer::WEB:
                return new ViewerWeb();
                break;
            case AbstractViewer::FILE:
                return new ViewerFile();
                break;
            case AbstractViewer::DB:
                return new ViewerDB();
                break;
            default:
                throw new \ErrorException("Not supprotet viever type");
                break;
        }
    }

}
