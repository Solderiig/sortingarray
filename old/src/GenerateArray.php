<?php

namespace Serhii\App;

class GenerateArray
{
    private int $size;

    public function __construct(int $size = null)
    {
        $this->size = $size;
    }

    public function getSize(): int
    {
        return $this->size;
    }

    public function generate(): array
    {
        for ($i = 0; $i < $this->getSize(); $i++) {
            for ($j = 0; $j < $this->getSize(); $j++) {
                $arResult[$i][$j] = rand(1, 1000);
            }
        }

        return $arResult;
    }
}