<?php

namespace Serhii\App\DB;

class Connection
{
    const HOST = "mysql";
    const USER = "crowdin";
    const DB = "crowdin";
    const PASS = "IKSGFuhjasd";

    private static $instance = null;

    public static function getInstace(): Connection
    {
        if (self::$instance == null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function connect(): \mysqli
    {
        $conn = new \mysqli($_ENV["HOST"], $_ENV["LOGIN"], $_ENV["PASS"], $_ENV["DB"]);

        if ($conn->connect_error) {
            throw new \ErrorException("Connection failed: " . $conn->connect_error);
        }

        return $conn;
    }

    public function __destruct()
    {
        global $DB;

        $DB->close();
    }

    private function __construct()
    {

    }

    private function __clone()
    {

    }
}