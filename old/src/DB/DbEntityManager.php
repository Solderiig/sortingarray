<?php

namespace Serhii\App\DB;

abstract class DbEntityManager
{
    protected \stdClass $query;

    abstract function getTableName();

    protected function reset(): void
    {
        $this->query = new \stdClass();
    }

    public function select(array $fields): DbEntityManager
    {
        $this->reset();
        $this->query->base = "SELECT " . implode(", ", $fields) . " FROM " . $this->getTableName();
        $this->query->type = 'select';

        return $this;
    }

    public function where(string $field, string $value, string $operator = '='): DbEntityManager
    {
        if (!in_array($this->query->type, ['select', 'update', 'delete'])) {
            throw new \Exception("WHERE can only be added to SELECT, UPDATE OR DELETE");
        }
        $this->query->where[] = "$field $operator '$value'";

        return $this;
    }

    public function limit(int $start, int $offset): DbEntityManager
    {
        if (!in_array($this->query->type, ['select'])) {
            throw new \Exception("LIMIT can only be added to SELECT");
        }
        $this->query->limit = " LIMIT " . $start . ", " . $offset;

        return $this;
    }

    public function getSQL(): string
    {
        $query = $this->query;
        $sql = $query->base;
        if (!empty($query->where)) {
            $sql .= " WHERE " . implode(' AND ', $query->where);
        }
        if (isset($query->limit)) {
            $sql .= $query->limit;
        }
        $sql .= ";";
        return $sql;
    }

    public function query()
    {
        $sql = $this->getSQL();

        global $DB;

        return $DB->Query($sql);
    }

    public function insert(array $data): DbEntityManager
    {
        $data = $this->prepareInser($data);

        $this->reset();
        $this->query->base = "INSERT INTO "
            . $this->getTableName()
            . " (" . implode(", ", array_keys($data)) . ") "
            . " VALUES (" . implode(", ", $data) . ")";
        $this->query->type = 'insert';

        return $this;
    }

    private function prepareInser(array $data): array
    {
        foreach ($data as $key => $datum) {
            if (is_array($datum)) {
                $data[$key] = "'" . json_encode($datum) . "'";
            } else {
                $data[$key] = "'" . $datum . "'";
            }
        }

        return $data;
    }
}