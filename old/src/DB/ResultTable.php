<?php

namespace Serhii\App\DB;

class ResultTable extends DbEntityManager
{
    function getTableName(): string
    {
        return "sort_result";
    }
}