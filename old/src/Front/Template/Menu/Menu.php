<ul class="nav">
    <?
    if (!empty($this->arResult)) {
        foreach ($this->arResult as $item) {
            ?>
            <li>
                <a class="nav-link" href="?action=<?= $item ?>">
                    <i class="pe-7s-note2"></i>
                    <p><?= $item ?></p>
                </a>
            </li>
            <?
        }
    }
    ?>
</ul>
