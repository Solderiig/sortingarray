<?php

foreach ($this->arResult["SORT_DATA"] as $sortertype) {
    ?>
    <div class="col-md-12">
        <div class="card">
            <div class="header">
                <h4 class="title"><?= $sortertype['NAME'] ?></h4>
            </div>

            <div class="content table-responsive table-full-width">
                <table class="table table-hover table-striped">
                    <tbody>
                        <tr>
                            <?php
                            foreach ($sortertype['DATA'] as $key => $item) {
                                ?>
                                <td><?= $item ?></td><?

                                if (($key + 1) % $sortertype['SIZE'] == 0) {
                                ?></tr>
                                    <tr><?
                                }
                            }
                            ?>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <?
}


