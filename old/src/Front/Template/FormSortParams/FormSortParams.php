<div class="container-fluid">
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <form id="paramsTypeSort">
                        <input type="hidden" name="action" value="setParams">
                        <div class="row">
                            <div class="col-md-5 pr-1">
                                <div class="form-group">
                                    <label>Type sort</label>
                                    <select multiple class="form-control" required name="typeSort[]">
                                        <?
                                        foreach ($this->arResult["TYPE_SORT"] as $item) {
                                            ?>
                                            <option value="<?= $item ?>"><?= $item ?></option><?
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3 px-1">
                                <div class="form-group">
                                    <label>Size</label>
                                    <div class="slidecontainer">
                                        <input type="range" min="<?= $this->arResult["SIZE"]["MIN"] ?>"
                                               max="<?= $this->arResult["SIZE"]["MAX"] ?>"
                                               value="<?= $this->arResult["SIZE"]["MAX"] / 2 ?>"
                                               class="slider"
                                               id="myRange"
                                               name="size"
                                        >
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-info btn-fill pull-right">Submit</button>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>