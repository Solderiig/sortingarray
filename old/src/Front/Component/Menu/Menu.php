<?php

namespace Serhii\App\Front\Component\Menu;

class Menu extends \Serhii\App\Front\Component\Component
{

    public function executeComponent()
    {
        $this->arResult = [
            "Web",
            "File",
            "DB",
        ];

        $this->includeTemplate();
    }

    protected function includeTemplate()
    {
        include $_SERVER["DOCUMENT_ROOT"] . "/src/Front/Template/Menu/Menu.php";
    }
}
