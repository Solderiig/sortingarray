<?php

namespace Serhii\App\Front\Component;

abstract class Component
{
    protected $arResult;

    abstract public function executeComponent();
    abstract protected function includeTemplate();
}