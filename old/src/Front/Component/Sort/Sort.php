<?php

namespace Serhii\App\Front\Component\Sort;

use Serhii\App\Buffer;
use Serhii\App\Sort\TypeSorter;
use Serhii\App\Front\Component\Component;

class Sort extends Component
{

    public function executeComponent()
    {
        $typeSort = new TypeSorter();
        $this->arResult["TYPE_SORT"] = $typeSort->getTypeSorter();

        $this->getHtml();

        $this->includeTemplate();
    }

    protected function includeTemplate()
    {
        include $_SERVER["DOCUMENT_ROOT"] . "/src/Front/Template/Sort/Sort.php";
    }

    private function getHtml(): void
    {
        foreach ($this->arResult["TYPE_SORT"] as $type) {
            $this->arResult["SORT_DATA"][$type] = $this->getbufferedhtml($type);
        }
    }

    private function getbufferedhtml($type): array
    {
        $buffered = Buffer::getInstace();
        return $buffered->getBuffer($type);
    }
}