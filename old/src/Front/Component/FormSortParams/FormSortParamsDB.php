<?php

namespace Serhii\App\Front\Component\FormSortParams;

class FormSortParamsDB extends FormSortParams
{
    protected function includeTemplate()
    {
        include $_SERVER["DOCUMENT_ROOT"] . "/src/Front/Template/FormSortParams/FormSortParamsDB.php";
    }
}