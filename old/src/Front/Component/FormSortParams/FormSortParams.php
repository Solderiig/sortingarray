<?php

namespace Serhii\App\Front\Component\FormSortParams;

use Serhii\App\Sort\TypeSorter;
use Serhii\App\Front\Component\Component;

class FormSortParams extends Component
{
    public function executeComponent()
    {
        $typeSort = new TypeSorter();
        $this->arResult["TYPE_SORT"] = $typeSort->getTypeSorter();

        $this->arResult["SIZE"]["MIN"] = 2;
        $this->arResult["SIZE"]["MAX"] = 10;

        $this->includeTemplate();
    }

    protected function includeTemplate()
    {
        include $_SERVER["DOCUMENT_ROOT"] . "/src/Front/Template/FormSortParams/FormSortParams.php";
    }
}