<?php

namespace Serhii\App\Front\Component\FormSortParams;

class FormSortParamsFile extends FormSortParams
{
    protected function includeTemplate()
    {
        include $_SERVER["DOCUMENT_ROOT"] . "/src/Front/Template/FormSortParams/FormSortParamsFile.php";
    }
}