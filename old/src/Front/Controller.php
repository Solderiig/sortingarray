<?php

namespace Serhii\App\Front;

class Controller
{
    const DEFAULT = "default";
    const WEB = "Web";
    const FILE = "File";
    const DB = "DB";
    const SETPARAMS = "setParams";
    const FOLDER_FRONT = "/public/";

    private string $type;

    public function __construct(string $type = self::DEFAULT)
    {
        $this->type = $type;
    }

    public function run(): void
    {
        switch ($this->type) {
            case self::DEFAULT:
            case self::WEB:
                $this->runDefault();
                break;
            case self::FILE:
                $this->runFile();
                break;
            case self::DB:
                $this->runDB();
                break;
            case self::SETPARAMS:
                $this->runAjaxSetParams();
                break;
        }
    }

    private function runDefault()
    {
        include $_SERVER["DOCUMENT_ROOT"] . self::FOLDER_FRONT . "index.php";
    }

    private function runAjaxSetParams()
    {
        include $_SERVER["DOCUMENT_ROOT"] . self::FOLDER_FRONT . "setParams.php";
    }

    private function runFile()
    {
        include $_SERVER["DOCUMENT_ROOT"] . self::FOLDER_FRONT . "indexFile.php";
    }

    private function runDB()
    {
        include $_SERVER["DOCUMENT_ROOT"] . self::FOLDER_FRONT . "indexDB.php";
    }
}