<?php

namespace Serhii\App;

class Sizer
{
    private int $size;
    private static $instance = null;

    public static function getInstance(): Sizer
    {
        if (self::$instance == null) {
            self::$instance = new Sizer();
        }

        return self::$instance;
    }

    public function setSize($size = null): void
    {
        if ($size == null) {
            $size = rand(2, 10);
        }

        $this->size = $size;
    }

    public function getSize(): int
    {
        return $this->size;
    }

    private function __construct()
    {

    }
}