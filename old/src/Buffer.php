<?php

namespace Serhii\App;

class Buffer
{
    private static $instance = null;

    private array $arBuffer;

    public static function getInstace(): Buffer
    {
        if (self::$instance == null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function addBuffer(string $id, array $data): void
    {
        $this->arBuffer[$id] = $data;
    }

    public function getBuffer(string $id): array
    {
        return $this->arBuffer[$id];
    }
}