<?php

namespace Serhii\App\Sort;

    abstract class FactorySorter
{
    public static function initial(string $name): Sorter
    {
        $className = "\\Serhii\\App\\Sort\\" . $name . "Sort";
        if (class_exists($className)) {
            $ob = new $className();
        }

        if ($ob instanceof Sorter) {
            return $ob;
        } else {
            throw new \ErrorException("Not supported type sorter $className");
        }
    }
}