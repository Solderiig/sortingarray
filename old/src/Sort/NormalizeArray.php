<?php

namespace Serhii\App\Sort;

class NormalizeArray
{
    private array $array;

    public function __construct(array $array)
    {
        $this->array = $array;
    }

    public function get(): array
    {
        $arResult = [];

        foreach ($this->array as $arItem) {
            $arResult = array_merge($arResult, $arItem);
        }

        $this->setArray($arResult);
        $this->sortNormalizeArray();

        return $this->array;
    }

    public function setArray(array $array): void
    {
        $this->array = $array;
    }

    private function sortNormalizeArray(): void
    {
        usort($this->array, function ($a, $b) {
            if ($a == $b) {
                return 0;
            }

            return ($a < $b) ? -1 : 1;
        });
    }

}