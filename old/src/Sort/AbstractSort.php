<?php

namespace Serhii\App\Sort;

abstract class AbstractSort
{
    protected array $array;
    protected int $size;

    public function setArray(array $array): void
    {
        $this->array = $array;
    }

    public function setSize(int $size): void
    {
        $this->size = $size;
    }

    protected function getArray(): array
    {
        return $this->array;
    }

    protected function getSize(): int
    {
        return $this->size;
    }

    protected function meargeArray(array $array): array
    {
        $arResult = [];

        foreach ($array as $arItem) {
            $arResult = array_merge($arResult, $arItem);
        }

        return $arResult;
    }
}