<?php

namespace Serhii\App\Sort;

class DiagonalSort extends AbstractSort implements Sorter
{
    public function sort(): array
    {
        $k = 0;
        $j = 0;
        $l = 1;
        $countOperation = 0;
        $maxCountOperation = 1;

        foreach ($this->array as $value) {
            $countOperation++;

            if ($countOperation == 1) {
                $k = $l < $this->getSize() ? $l - 1 : $this->getSize() - 1;
                $j = 0;

                if ($l > $this->getSize()) {
                    $j = $this->getSize() - ($this->getSize() * 2 - $l);
                }
            }

            $arTmpRes[$k][$j] = $value;

            if ($countOperation == $maxCountOperation) {
                $l++;
                $countOperation = 0;
                if ($l > $this->getSize()) {
                    $maxCountOperation--;
                } else {
                    $maxCountOperation++;
                }
            } else if ($countOperation <= $maxCountOperation) {
                $k--;
                $j++;
            }
        }

        return $this->meargeArray($arTmpRes);
    }
}