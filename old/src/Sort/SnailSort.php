<?php

namespace Serhii\App\Sort;

class SnailSort extends AbstractSort implements Sorter
{
    private int $tempSize;
    private string $xDirectionVal;
    private string $yDirectionVal;
    private string $direction;

    /**
     * SnailSort constructor.
     */
    public function __construct()
    {
        $this->xDirectionVal = "+";
        $this->yDirectionVal = "+";
        $this->direction = "x";
    }

    public function sort(): array
    {
        $this->tempSize = $this->getSize();

        $y = 0;
        $x = 0;
        $countOperation = 0;

        foreach ($this->array as $item) {
            $countOperation++;
            $arTmpRes[$y][$x] = $item;

            if ($countOperation == $this->tempSize && $this->direction == 'x') {
                $countOperation = 0;
                $this->tempSize--;

                $this->changeDirection();
                $this->changeXYDirectionVal("x");
            }

            if ($countOperation == $this->tempSize && $this->direction == 'y') {
                $countOperation = 0;

                $this->changeDirection();
                $this->changeXYDirectionVal("y");
            }

            if ($this->direction == 'x') {
                $this->changeCoord($x, "x");
            } else {
                $this->changeCoord($y, "y");
            }
        }

        return $this->meargeArray($arTmpRes);
    }

    private function changeCoord(int &$coord, string $type): void
    {
        $var = $type . "DirectionVal";
        $val = $this->$var;

        if ($val == "+") {
            $coord++;
        } else {
            $coord--;
        }
    }

    private function changeDirection(): void
    {
        if ($this->direction == 'y') {
            $this->direction = 'x';
        } else {
            $this->direction = 'y';
        }
    }

    private function changeXYDirectionVal(string $type): void
    {
        $var = $type . "DirectionVal";

        if ($this->$var == '-') {
            $this->$var = '+';
        } else {
            $this->$var = '-';
        }
    }

    protected function meargeArray(array $array): array
    {
        $arResult = [];

        foreach ($array as $arItem) {
            ksort($arItem);
            $arResult = array_merge($arResult, $arItem);
        }

        return $arResult;
    }
}