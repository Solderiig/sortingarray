<?php

namespace Serhii\App\Sort;

interface Sorter
{
    public function sort();
}