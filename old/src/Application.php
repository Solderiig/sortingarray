<?php

namespace Serhii\App;

use Serhii\App\Sort\TypeSorter,
    Serhii\App\Sort\NormalizeArray,
    Serhii\App\DB\TypeSorterTable,
    Serhii\App\DB\Connection,
    Serhii\App\Viewer\Observer,
    Serhii\App\Viewer\AbstractViewer;
use Serhii\App\Viewer\FactoryViewer;

class Application
{
    private Observer $observerViewed;
    private int $size;
    private array $array;
    private array $normalizeArray;

    public function run(): void
    {
        $this->size = $this->getSize();
        $this->array = $this->generateArray();
        $this->normalizeArray();

        $this->coreRun();

        $this->sort();

        $this->frontAction();
    }

    private function getSize(): int
    {
        $size = Sizer::getInstance();

        $sizeValue = null;

        if ($_REQUEST["size"] != "") {
            $sizeValue = $_REQUEST["size"];
        }

        $size->setSize($sizeValue);
        $intSize = $size->getSize();

        return $intSize;
    }

    private function generateArray(): array
    {
        $generateArray = new GenerateArray($this->size);
        $array = $generateArray->generate();

        return $array;
    }

    private function normalizeArray(): void
    {
        $normalize = new NormalizeArray($this->array);
        $this->normalizeArray = $normalize->get();
    }

    private function setViewer(): void
    {
        $observer = new Observer();

        try {
            switch ($_REQUEST["action"]) {
                case "File":
                    $viewerFile = FactoryViewer::initial(AbstractViewer::FILE);
                    $viewerFile->setSize($this->size);
                    $viewerFile->setDefaultArray($this->array);
                    $observer->attach($viewerFile);
                    break;
                case "DB":
                    if (!empty($_REQUEST["send"])) {
                        $viewerDB = FactoryViewer::initial(AbstractViewer::DB);
                        $viewerDB->setSize($this->size);
                        $viewerDB->setDefaultArray($this->array);
                        $observer->attach($viewerDB);
                    }
                    break;
                case "ALL":
                    $viewerWeb = FactoryViewer::initial(AbstractViewer::WEB);
                    $viewerWeb->setSize($this->size);
                    $viewerWeb->setDefaultArray($this->array);
                    $observer->attach($viewerWeb);

                    $viewerFile = FactoryViewer::initial(AbstractViewer::FILE);
                    $viewerFile->setSize($this->size);
                    $viewerFile->setDefaultArray($this->array);
                    $observer->attach($viewerFile);

                    $viewerDB = FactoryViewer::initial(AbstractViewer::DB);
                    $viewerDB->setSize($this->size);
                    $viewerDB->setDefaultArray($this->array);
                    $observer->attach($viewerDB);
                    break;
                default:
                    $viewerWeb = FactoryViewer::initial(AbstractViewer::WEB);
                    $viewerWeb->setSize($this->size);
                    $viewerWeb->setDefaultArray($this->array);
                    $observer->attach($viewerWeb);
                    break;
            }
        } catch (\ErrorException $e) {
            echo 'Помилка виводу: ', $e->getMessage(), "\n";
        };

        $this->observerViewed = $observer;
    }

    private function sort(): void
    {
        $sorter = new TypeSorter();
        $sorter->runSort($this->normalizeArray, $this->observerViewed, $this->size);
    }

    private function connectDB()
    {
        $connection = Connection::getInstace();

        global $DB;
        try {
            $DB = $connection->connect();
        } catch (\ErrorException $e) {
            echo 'Помилка з\'єднання: ', $e->getMessage(), "\n";
        }
    }

    private function coreRun(): void
    {
        $this->connectDB();
        $this->setViewer();
    }

    private function frontAction()
    {
        $action = "Web";

        if ($_REQUEST["action"] != '') {
            $action = $_REQUEST["action"];
        }

        $frontController = new Front\Controller($action);
        $frontController->run();
    }
}