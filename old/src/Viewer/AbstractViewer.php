<?php

namespace Serhii\App\Viewer;

abstract class  AbstractViewer
{
    const WEB = "web";
    const FILE = "file";
    const DB = "DB";

    protected int $size;
    protected array $defaultArray;

    abstract public function print(array $array, string $sorter): void;

    public function getDefaultArray(): array
    {
        return $this->defaultArray;
    }

    public function setDefaultArray(array $defaultArray): void
    {
        $this->defaultArray = $defaultArray;
    }

    public function setSize(int $size): void
    {
        $this->size = $size;
    }

    protected function GetSize(): int
    {
        return $this->size;
    }
}