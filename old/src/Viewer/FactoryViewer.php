<?php

namespace Serhii\App\Viewer;

class FactoryViewer
{
    public static function initial(string $type): AbstractViewer
    {
        if ($type == AbstractViewer::WEB) {
            return new ViewerWeb();
        } else if ($type == AbstractViewer::FILE) {
            return new ViewerFile();
        } else if ($type == AbstractViewer::DB) {
            return new ViewerDB();
        } else {
            throw new \ErrorException("Not supprotet viever type");
        }
    }

}