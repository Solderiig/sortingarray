<?php

namespace Serhii\App\Viewer;

use Serhii\App\Buffer;

class ViewerWeb extends AbstractViewer
{
    public function print($array, string $sorter): void
    {
        $data = [
            'NAME' => $sorter,
            'SIZE' => $this->GetSize(),
            'DATA' => $array,
        ];

        $this->buffered($sorter, $data);
    }

    private function buffered(string $sorter, array $string): void
    {
        $buffer = Buffer::getInstace();
        $buffer->addBuffer($sorter, $string);
    }
}