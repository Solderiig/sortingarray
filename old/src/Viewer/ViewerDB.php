<?php

namespace Serhii\App\Viewer;

use \Serhii\App\DB\ResultTable;

class ViewerDB extends AbstractViewer
{
    private ResultTable $DB;

    public function __construct()
    {
        $this->DB = new ResultTable();
    }

    public function print(array $array, string $sorter): void
    {
        $arData = [
            "in_array" => $this->getDefaultArray(),
            "out_array" => $array,
            "type_sorter" => $sorter,
        ];
        $this->DB
            ->insert($arData)
            ->query();
    }
}