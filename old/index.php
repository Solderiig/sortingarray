<?php
include "vendor/autoload.php";

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$application = new \Serhii\App\Application ();
$application->run();



