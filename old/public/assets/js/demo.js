type = ['', 'info', 'success', 'warning', 'danger'];


demo = {

    showNotification: function (from, align) {
        color = Math.floor((Math.random() * 4) + 1);
        $.notify({
            icon: "pe-7s-gift",
            message: "Load success"

        }, {
            type: type[2],
            timer: 4000,
            placement: {
                from: from,
                align: align
            }
        });
    }


}

window.addEventListener("load", function () {
    function sendData() {
        const XHR = new XMLHttpRequest();

        const FD = new FormData(form);
        XHR.addEventListener("load", function (event) {
            var el = document.querySelector('#sorter-result');
            el.innerHTML = event.target.responseText;

            demo.showNotification('top', 'right')
        });
        XHR.open("POST", "/index.php");
        XHR.send(FD);
    }

    const form = document.getElementById("paramsTypeSort");

    if (form != null) {
        form.addEventListener("submit", function (event) {
            event.preventDefault();

            sendData();
        });
    }


    function sendDataForm(formDataDOM) {
        const XHR = new XMLHttpRequest();

        const FD = new FormData(formDataDOM);
        XHR.addEventListener("load", function (event) {
            demo.showNotification('top', 'right')
        });
        XHR.open("POST", "/index.php");
        XHR.send(FD);
    }

    const formFile = document.getElementById("DownloadFile");
    if (formFile != null) {
        formFile.addEventListener("submit", function (event) {
            event.preventDefault();

            sendDataForm(formFile);
        });
    }

    const formDB = document.getElementById("saveDB");
    if (formDB != null) {
        formDB.addEventListener("submit", function (event) {
            event.preventDefault();

            sendDataForm(formDB);
        });
    }
});


