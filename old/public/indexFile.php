<?php

use \Serhii\App\Front\Component;

include "header.php";

$component = new Component\FormSortParams\FormSortParamsFile();
$component->executeComponent();
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <!-- Standard button -->
                    <a href="/file/file_result.txt" download class="download-file">
                        <button type="button" class="btn btn-default">Download</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<?php

include "footer.php";