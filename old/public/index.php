<?php

use \Serhii\App\Front\Component;

include "header.php";

$component = new Component\FormSortParams\FormSortParams();
$component->executeComponent();
?>

<div class="container-fluid">
    <div class="row" id="sorter-result">
        <?
        $component = new Component\Sort\Sort();
        $component->executeComponent();
        ?>
    </div>
</div>
<?php

include "footer.php";