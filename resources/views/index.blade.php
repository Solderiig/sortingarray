@extends("layout.app")


@section("filter")
    <x-filter formID="paramsTypeSort"></x-filter>
@endsection

@section("content")
    <div class="container-fluid">
        <div class="row" id="sorter-result">
            <x-sort></x-sort>
        </div>
    </div>
@endsection
