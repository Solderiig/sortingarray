<div class="container-fluid">
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <form id="{{$idForm}}">
                        <input type="hidden" name="action" value="setParams">
                        <input type="hidden" name="send" value="send">
                        <div class="row">
                            <div class="col-md-5 pr-1">
                                <div class="form-group">
                                    <label>Type sort</label>
                                    <select multiple class="form-control" required name="typeSort[]">
                                        @foreach ($arResult["TYPE_SORT"] as $item)
                                            <option value="{{$item}}">{{$item}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3 px-1">
                                <div class="form-group">
                                    <label>Size</label>
                                    <div class="slidecontainer">
                                        <input type="range" min="{{$arResult["SIZE"]["MIN"]}}"
                                               max="{{$arResult["SIZE"]["MAX"]}}"
                                               value="{{$arResult["SIZE"]["MAX"] / 2 }}"
                                               class="slider"
                                               id="myRange"
                                               name="size"
                                        >
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-info btn-fill pull-right">Submit</button>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
