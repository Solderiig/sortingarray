<ul class="nav">
    @foreach ($arResult as $item)
        <li>
            <a class="nav-link" href="/{{$item}}">
                <i class="pe-7s-note2"></i>
                <p>{{$item}}</p>
            </a>
        </li>
    @endforeach
</ul>
