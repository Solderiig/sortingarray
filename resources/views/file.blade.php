@extends("layout.app")


@section("filter")
    <x-filter formID="File"></x-filter>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <!-- Standard button -->
                        <a href="/file/file_result.txt" download class="download-file">
                            <button type="button" class="btn btn-default">Download</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

