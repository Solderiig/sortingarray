require('./bootstrap');

import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App'
import {
    BrowserRouter,
    Routes,
    Route,
} from "react-router-dom";
import Home from "./Page/Home";
import FilePage from "./Page/FilePage";
//


ReactDOM.render(
    <React.StrictMode>
        <BrowserRouter>
            <div className="wrapper">
                <App/>
            </div>
        </BrowserRouter>
    </React.StrictMode>
    ,
    document.getElementById('root')
);
