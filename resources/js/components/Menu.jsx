import React, {useState} from 'react'
import axios from "axios";
import {Link} from 'react-router-dom'

const Menu = () => {
    const [menus, setMenu] = useState([])

    React.useEffect(() => {
        const getData = async () => {
            const {data} = await axios.get("/menu.json.get");

            setMenu(data)
        }
        getData();
    }, [])

    return (
        <ul className="nav">
            {menus.map((menu, idx) => (
                <li key={idx}>
                    <Link to={menu} className="nav-link">
                        <i className="pe-7s-note2"></i>
                        <p>{menu}</p>
                    </Link>
                </li>
            ))}
        </ul>

    )
}

export default Menu
