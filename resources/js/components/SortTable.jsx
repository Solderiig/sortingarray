import React from 'react'

const SortTable = (props) => {
    return (
        <div className="content table-responsive table-full-width">
            <table className="table table-hover table-striped">
                <tbody>
                {props.data.map((row, idx) => (
                    <tr key={idx}>
                        {row.map((cell, idxc) => (
                            <td key={idxc}>{cell}</td>
                        ))}
                    </tr>
                ))}
                </tbody>
            </table>
        </div>

    )
}

export default SortTable
