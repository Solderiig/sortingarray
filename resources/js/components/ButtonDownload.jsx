import React, {useState} from 'react'
import axios from "axios";
import {Link} from 'react-router-dom'

const ButtonDownload = () => {
    return (
        <div className="container-fluid">
            <div className="row">
                <div className="col-md-12">
                    <div className="card">
                        <div className="card-body">
                            <a href="/file/file_result.txt" download className="download-file">
                                <button type="button" className="btn btn-default">Download</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ButtonDownload
