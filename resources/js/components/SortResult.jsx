import React, {useState} from 'react'
import Filter from "./Filter";
import SortTable from "./SortTable";
import axios from "axios";

const SortResult = (props) => {

    const [sorts, setSorts] = useState({SORT_DATA: []});

    React.useEffect(() => {
        const getData = async () => {
            const {data} = await axios.post("/sort.json.get");
            setSorts(data)
        }
        getData();
    }, [])

    return (
        <div>
            <Filter formID={props.formID} stateSort={setSorts}/>
            <div className="container-fluid">
                <div className="row" id="sorter-result">
                    {Object.keys(sorts.SORT_DATA).map((keyName, idx) => (
                        <div key={idx} className="col-md-12">
                            <div className="card">
                                <div className="header">
                                    <h4 className="title">{sorts.SORT_DATA[keyName].NAME}</h4>
                                </div>
                                <SortTable data={sorts.SORT_DATA[keyName].DATA} size={sorts.SORT_DATA[keyName].SIZE}/>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        </div>
    )
}

export default SortResult
