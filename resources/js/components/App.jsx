import React from 'react'
import {BrowserRouter, Route, Routes} from "react-router-dom";
import Home from "../Page/Home";
import FilePage from "../Page/FilePage";
import Db from "../Page/Db";
import Menu from './Menu'

class App extends React.Component {
    render() {
        return (
            <div className="wrapper">
                <div className="sidebar" data-color="purple" data-image="/assets/img/sidebar-5.jpg">
                    <div className="sidebar-wrapper">
                        <div className="logo">
                            <a href="http://www.creative-tim.com" className="simple-text">
                                Creative Tim
                            </a>
                        </div>
                        <Menu/>
                    </div>
                </div>
                <div className="main-panel">
                    <nav className="navbar navbar-default navbar-fixed">
                        <div className="container-fluid">
                            <div className="navbar-header">
                                <button type="button" className="navbar-toggle" data-toggle="collapse"
                                        data-target="#navigation-example-2">
                                    <span className="sr-only">Toggle navigation</span>
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                </button>
                                <a className="navbar-brand" href="#">Table List</a>
                            </div>
                            <div className="collapse navbar-collapse">
                            </div>
                        </div>
                    </nav>

                    <div className="content">
                        <Routes>
                            <Route path='/' exact={true}>
                                <Route index element={<Home/>}/>
                            </Route>
                            <Route path="/Web" element={<Home/>}/>
                            <Route path='/File' exact={true}>
                                <Route index element={<FilePage/>}/>
                            </Route>
                            <Route path='/Db' exact={true}>
                                <Route index element={<Db/>}/>
                            </Route>
                        </Routes>
                    </div>
                </div>
            </div>
        );
    }
}

export default App
