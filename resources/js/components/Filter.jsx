import React, {useState} from 'react'
import axios from "axios";

const Filter = (props) => {
    const [range, setRenge] = useState(5);
    const [typeSort, setTypeSort] = useState([]);

    function changeRange(event) {
        setRenge(event.target.value);
    }

    function changetypeSort(event) {
        var options = event.target.options;
        var value = [];
        for (var i = 0, l = options.length; i < l; i++) {
            if (options[i].selected) {
                value.push(options[i].value);
            }
        }
        setTypeSort(value);
    }

    async function handleFilter(event) {
        var body = {
            action: props.formID,
            send: 'send',
            typeSort: typeSort,
            size: range,
        }


        axios({
            method: 'post',
            url: '/sort.json.get',
            data: body,
        })
            .then(function (response) {
                if (typeof props.stateSort != "undefined") {
                    props.stateSort(response.data)
                }
            });

        event.preventDefault();
    }

    return (
        <div className="container-fluid">
            <div className="row">
                <div className="col-md-8">
                    <div className="card">
                        <div className="card-body">
                            <form id={props.formID} onSubmit={handleFilter}>
                                <input type="hidden" name="action" value={props.formID}/>
                                <input type="hidden" name="send" value="send"/>
                                <div className="row">
                                    <div className="col-md-5 pr-1">
                                        <div className="form-group">
                                            <label>Type sort</label>
                                            <select multiple={true} className="form-control" required=""
                                                    name="typeSort[]" onChange={changetypeSort}>
                                                <option value="Horizontal">Horizontal</option>
                                                <option value="Vertical">Vertical</option>
                                                <option value="Snake">Snake</option>
                                                <option value="Diagonal">Diagonal</option>
                                                <option value="Snail">Snail</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className="col-md-3 px-1">
                                        <div className="form-group">
                                            <label>Size</label>
                                            <div className="slidecontainer">
                                                <input type="range" min="2" max="10" value={range}
                                                       onChange={changeRange}
                                                       className="slider" id="myRange" name="size"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" className="btn btn-info btn-fill pull-right">Submit
                                </button>
                                <div className="clearfix"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    )
}

export default Filter
