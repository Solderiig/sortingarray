import React from 'react'
import Filter from "../components/Filter";
import ButtonDownload from "../components/ButtonDownload";


const FilePage = () => {
    return (
        <div>
            <Filter formID='File'/>
            <ButtonDownload/>
        </div>
    )
}

export default FilePage
