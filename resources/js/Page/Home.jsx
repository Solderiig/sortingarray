import React from 'react'
import Filter from "../components/Filter";
import SortResult from "../components/SortResult"

const Home = () => {
    return (
        <div>
            <SortResult formID="paramsTypeSort"/>
        </div>
    )
}

export default Home
