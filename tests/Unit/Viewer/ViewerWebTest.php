<?php

namespace Tests\Unit;

use App\Buffer;
use App\GenerateArray;
use App\Sort\FactorySorter;
use App\Sort\NormalizeArray;
use App\Viewer\FactoryViewer;
use App\Viewer\AbstractViewer;
use PHPUnit\Framework\TestCase;

class ViewerWebTest extends TestCase
{
    public function testViewerWeb()
    {
        $viewerDB = FactoryViewer::initial(AbstractViewer::WEB);

        $array = $this->getArray();

        $viewerDB->setSize(3);
        $viewerDB->setDefaultArray($this->defaultArray);
        $viewerDB->print($array, "Horizontal");

        $buffer = Buffer::getInstace();
        $arrayBuffer = $buffer->getBuffer("Horizontal");

        $this->assertEquals($array, $arrayBuffer["DATA"]);
    }

    private function getArray(): array
    {
        $size = 3;

        $generateArray = new GenerateArray($size);
        $array = $generateArray->generate();

        $normalize = new NormalizeArray($array);
        $this->defaultArray = $array = $normalize->get();

        $sorter = FactorySorter::initial("Horizontal");
        $sorter->setArray($array);
        $sorter->setSize($size);

        $array = $sorter->sort();

        return $array;
    }

}
