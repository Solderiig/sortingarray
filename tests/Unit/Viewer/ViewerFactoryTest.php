<?php

namespace Tests\Unit;

use App\Viewer\ViewerDB;
use App\Viewer\ViewerWeb;
use App\Viewer\ViewerFile;
use App\Viewer\FactoryViewer;
use App\Viewer\AbstractViewer;
use PHPUnit\Framework\TestCase;

class ViewerFactoryTest extends TestCase
{
    public function testViewerFile()
    {
        $viewerFile = FactoryViewer::initial(AbstractViewer::FILE);

        $this->assertInstanceOf(ViewerFile::class, $viewerFile);
        $this->assertInstanceOf(AbstractViewer::class, $viewerFile);
    }

    public function testViewerDB()
    {
        $viewerDB = FactoryViewer::initial(AbstractViewer::DB);

        $this->assertInstanceOf(ViewerDB::class, $viewerDB);
        $this->assertInstanceOf(AbstractViewer::class, $viewerDB);
    }

    public function testViewerWeb()
    {
        $viewerWeb = FactoryViewer::initial(AbstractViewer::WEB);

        $this->assertInstanceOf(ViewerWeb::class, $viewerWeb);
        $this->assertInstanceOf(AbstractViewer::class, $viewerWeb);
    }

    public function testNotSupportedViewer()
    {
        $this->expectException(\ErrorException::class);
        $viewerWeb = FactoryViewer::initial("notSupportedType");
    }
}
