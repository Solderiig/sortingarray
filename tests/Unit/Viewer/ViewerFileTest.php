<?php

namespace Tests\Unit;

use App\GenerateArray;
use App\Sort\FactorySorter;
use App\Sort\NormalizeArray;
use App\Viewer\FactoryViewer;
use App\Viewer\AbstractViewer;
use PHPUnit\Framework\TestCase;

class ViewerFileTest extends TestCase
{
    public function setUp(): void
    {
        $_SERVER['DOCUMENT_ROOT'] = __DIR__ . "/../../../public";
    }

    public function testWritebleFile()
    {
        $this->assertFileIsWritable(__DIR__ . "/../../../public/file/file_result.txt");
    }

    /**
     * @depends testWritebleFile
     */
    public function testViewerFile()
    {
        $viewerDB = FactoryViewer::initial(AbstractViewer::FILE);

        $array = $this->getArray();

        $viewerDB->setSize(3);
        $viewerDB->setDefaultArray($this->defaultArray);

        try {
            $viewerDB->print($array, "Horizontal");
        } catch (\ErrorException $e) {
            $this->fail("error file viewer");
        };

        $this->assertTrue(true);
    }

    private function getArray(): array
    {
        $size = 3;

        $generateArray = new GenerateArray($size);
        $array = $generateArray->generate();

        $normalize = new NormalizeArray($array);
        $this->defaultArray = $array = $normalize->get();

        $sorter = FactorySorter::initial("Horizontal");
        $sorter->setArray($array);
        $sorter->setSize($size);

        $array = $sorter->sort();

        return $array;
    }

    /**
     * @depends testViewerFile
     */

    public function testFile()
    {
        $this->assertFileExists(__DIR__ . "/../../../public/file/file_result.txt");
        $this->assertFileIsReadable(__DIR__ . "/../../../public/file/file_result.txt");
    }

}
