<?php

namespace Tests\Unit;

use App\Sizer;
use App\GenerateArray;
use App\Sort\NormalizeArray;
use PHPUnit\Framework\TestCase;

class NormalizeTest extends TestCase
{
    public function dataProvider()
    {
        return [
            "2s" => [
                [
                    [2, 4],
                    [1, 3],
                ],
                [1, 2, 3, 4],
            ],
            "3s" => [
                [
                    [7, 2, 6],
                    [1, 3, 5],
                    [8, 9, 4],
                ],
                [1, 2, 3, 4, 5, 6, 7, 8, 9],
            ],
            "4s" => [
                [
                    [12, 5, 2, 3],
                    [15, 11, 6, 1],
                    [4, 7, 16, 10],
                    [8, 9, 13, 14],
                ],
                [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16],
            ],
        ];
    }

    /**
     * @dataProvider dataProvider()
     * */

    public function testNormilizeArray($in, $out)
    {
        $normalize = new NormalizeArray($in);
        $array = $normalize->get();

        $this->assertEquals($out, $array);
    }
}
