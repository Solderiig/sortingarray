<?php

namespace Tests\Unit;

use App\Sizer;
use App\Sort\Sorter;
use App\GenerateArray;
use App\Sort\FactorySorter;
use App\Sort\NormalizeArray;
use PHPUnit\Framework\TestCase;

class FactorySortTest extends TestCase
{
    public function dataProvider()
    {
        return [
            ["Horizontal"],
            ["Vertical"],
            ["Snake"],
            ["Diagonal"],
            ["Snail"],
        ];
    }

    /**
     * @dataProvider dataProvider()
     * */

    public function testCreateObSorter($type)
    {
        $sorter = FactorySorter::initial($type);

        $this->assertInstanceOf(Sorter::class, $sorter);
    }

    public function testNotValidSorter()
    {
        $this->expectException(\ErrorException::class);
        $sorter = FactorySorter::initial("notValidSorter");
    }
}
