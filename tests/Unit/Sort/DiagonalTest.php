<?php

namespace Tests\Unit;

use App\Sizer;
use App\GenerateArray;
use App\Sort\FactorySorter;
use App\Sort\NormalizeArray;
use PHPUnit\Framework\TestCase;

class DiagonalTest extends TestCase
{
    public function dataProvider()
    {
        return [
            "2s" => [
                [
                    [2, 4],
                    [1, 3],
                ],
                [1, 3, 2, 4],
            ],
            "3s" => [
                [
                    [7, 2, 6],
                    [1, 3, 5],
                    [8, 9, 4],
                ],
                [1, 3, 6, 2, 5, 8, 4, 7, 9],
            ],
            "4s" => [
                [
                    [12, 5, 2, 3],
                    [15, 11, 6, 1],
                    [4, 7, 16, 10],
                    [8, 9, 13, 14],
                ],
                [1, 3, 6, 10, 2, 5, 9, 13, 4, 8, 12, 15, 7, 11, 14, 16],
            ],
        ];
    }

    /**
     * @dataProvider dataProvider()
     * */

    public function testHorizontalTest($in, $out)
    {
        $normalize = new NormalizeArray($in);
        $array = $normalize->get();
        $sorter = FactorySorter::initial("Diagonal");

        $sorter->setArray($array);
        $sorter->setSize(count($in));

        $array = $sorter->sort();

        $this->assertEquals($out, $array);
    }
}
