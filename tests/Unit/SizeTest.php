<?php

namespace Tests\Feature;

use App\Sizer;
use Tests\TestCase;

class SizeTest extends TestCase
{
    public function testRandSize()
    {
        $size = Sizer::getInstance();
        $size->setSize();
        $intSize = $size->getSize();

        $this->assertLessThanOrEqual(10, $intSize);
        $this->assertGreaterThanOrEqual(2, $intSize);

    }

    public function dataProvider()
    {
        return [
            [-1],
            [0],
            [1],
            [2],
            [5],
            [9],
            [10],
            [11],
        ];
    }

    /**
     * @dataProvider dataProvider()
     * */
    public function testFixSize($sizeparam)
    {
        $size = Sizer::getInstance();
        $size->setSize($sizeparam);
        $intSize = $size->getSize();

        $this->assertLessThanOrEqual(10, $intSize);
        $this->assertGreaterThanOrEqual(2, $intSize);
    }

}
