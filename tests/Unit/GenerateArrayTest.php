<?php

namespace Tests\Unit;

use App\Sizer;
use App\GenerateArray;
use PHPUnit\Framework\TestCase;

class GenerateArrayTest extends TestCase
{
    private $size;

    protected function setUp(): void
    {
        parent::setUp();

        $size = Sizer::getInstance();
        $size->setSize(5);
        $this->size = $size->getSize();

    }

    public function testGenerateArraySetSize()
    {
        $generateArray = new GenerateArray($this->size);
        $array = $generateArray->generate();

        $this->assertCount($this->size, $array);

        foreach ($array as $item) {
            $this->assertCount($this->size, $item);
        }
    }

    public function testNotSetSize()
    {
        $this->expectException(\ErrorException::class);
        $generateArray = new GenerateArray();
        $array = $generateArray->generate();
    }
}
