<?php

namespace Tests\Feature;

use App\Models\Result;
use App\GenerateArray;
use App\Sort\FactorySorter;
use App\Sort\NormalizeArray;
use App\Viewer\FactoryViewer;
use App\Viewer\AbstractViewer;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ViewerDBTest extends TestCase
{

    use RefreshDatabase;

    public function testViewerDB()
    {
        $viewerDB = FactoryViewer::initial(AbstractViewer::DB);

        $array = $this->getArray();

        $viewerDB->setSize(3);
        $viewerDB->setDefaultArray($this->defaultArray);
        $viewerDB->print($array, "Horizontal");

        $all = Result::find(1);

        $this->assertEquals(json_encode($array), $all->out_array);
    }

    private function getArray(): array
    {
        $size = 3;

        $generateArray = new GenerateArray($size);
        $array = $generateArray->generate();

        $normalize = new NormalizeArray($array);
        $this->defaultArray = $array = $normalize->get();

        $sorter = FactorySorter::initial("Horizontal");
        $sorter->setArray($array);
        $sorter->setSize($size);

        $array = $sorter->sort();

        return $array;
    }
}
