<?php

namespace Tests\Feature;

use App\Sort\TypeSorter;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TypeSortTest extends TestCase
{
    /**
     * @var array
     */
    private $typeSorter;

    public function setUp(): void
    {
        parent::setUp();

        $this->typeSorter = [
            "Horizontal",
            "Vertical",
            "Snake",
            "Diagonal",
            "Snail",
        ];
    }

    public function testTypeSortFull()
    {

        $type = new TypeSorter();
        $types = $type->getTypeSorter();

        $this->assertEquals($this->typeSorter, $types);
    }

    public function testTypeSortRequest()
    {
        request()->merge([
            'typeSort' => [
                "Horizontal",
                "Vertical",
            ],
        ]);

        $type = new TypeSorter();
        $types = $type->getTypeSorter();

        $this->assertEquals([
            "Horizontal",
            "Vertical",
        ], $types);
    }
}
