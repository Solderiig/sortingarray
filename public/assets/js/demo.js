type = ['', 'info', 'success', 'warning', 'danger'];


demo = {

    showNotification: function (from, align) {
        color = Math.floor((Math.random() * 4) + 1);
        $.notify({
            icon: "pe-7s-gift",
            message: "Load success"

        }, {
            type: type[2],
            timer: 4000,
            placement: {
                from: from,
                align: align
            }
        });
    }


}

window.addEventListener("load", function () {
    function sendData() {
        $.ajax({
            url:"/setParams",
            method:"GET",
            data:$(form).serialize(),
            success:function (data) {
                $("#sorter-result").html(data)
                demo.showNotification('top', 'right')
            }
        })
    }

    const form = document.getElementById("paramsTypeSort");

    if (form != null) {
        form.addEventListener("submit", function (event) {
            event.preventDefault();

            sendData();
        });
    }


    function sendDataForm(formDataDOM,url) {

        $.ajax({
            url:"/" + url,
            method:"GET",
            data:$(formDataDOM).serialize(),
            success:function (data) {

            }
        })
    }

    const formFile = document.getElementById("File");
    if (formFile != null) {
        formFile.addEventListener("submit", function (event) {
            event.preventDefault();

            sendDataForm(formFile,"File");
        });
    }

    const formDB = document.getElementById("DB");
    if (formDB != null) {
        formDB.addEventListener("submit", function (event) {
            event.preventDefault();

            sendDataForm(formDB,"DB");
        });
    }
});


