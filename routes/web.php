<?php

use App\Crowdin\App;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Application;
use \App\View\Components\Menu;
use \App\Http\Controllers\GetJsonSort;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* public */
Route::get('/setParams', [Application::class, "run"])->name("setParams");
Route::get('/menu.json.get', [Menu::class, "getJson"])->name("getMenuJson");
Route::post('/sort.json.get', [GetJsonSort::class, "getJson"])->name("getSortJson");

/* crowdin app core */
Route::get('/manifest.json', [App::class, "manifest"]);
Route::get('/install', [App::class, "event"]);
Route::get('/uninstall', [App::class, "event"]);

/* all */
Route::get("/{type?}", function () {
    return view("react");
})->name("React");
